#include <iostream>

struct Vec2D{
  double x = 0, y = 0;
};

struct Node {
  Vec2D value; // unsere Data sind vom Typ Vec2D
  Node* next = nullptr; // Pointer auf naechsten Node
};

class Queue{
  private:
    Node* first; // erster Node in der Queue
    Node* last; // letzter Node in der Queue
    unsigned int size; // Speichert die Groesse der Queue
    std::string name; // Name um, Ausgaben zu unterscheiden
    
    void deleteNodes(Node* start); // Loescht alle Nodes ab dem Start.
    void printNodes(Node* start, std::ostream& out); // Gibt alle Nodes ab start an out aus.
  
  public:
    // Default Konstruktor
    // s : Name
    Queue(std::string name);
    
    // Destruktor
    // loescht alle dynamisch allozierten Werte
    ~Queue();
    
    // kopiert alle dynamisch allozierten Werte
    Queue(const Queue& other);
    
    // weis dieser Queue die Queue other zu
    Queue& operator=(const Queue& other);
    
    // POST: Gibt die Groesse der Liste zurueck
    unsigned int getSize();
    
    // POST: Gibt den Namen der Queue zurueck
    std::string getName();
    
    // PRE: valid ostream
    // POST: gibt die ganze Queue an den Stream aus
    void print(std::ostream& out);
    
    // PRE: gueltiger Vec2D
    // POST: Fuege Element zu Queue hinzu (neuer Last Node)
    void enqueue(Vec2D input);
    
    // PRE: Queue nicht leer
    // POST Loesche letztes Element aus der Queue und gib dessen Wert zurueck.
    Vec2D dequeue();
    
    // PRE: Gueltige Queue
    // POST: Setzt zwei Queues zusammen. Haengt Kopie der Werte an und gibt neue Queue zurueck.
    Queue operator+(const Queue& other);
    
    
    // PRE: Gueltige Queue
    // POST: Haengt Kopie der Werte von other an diese Queue.
    Queue& operator+=(const Queue& other);
};

// PRE: 2D-Vektor wird so eingegeben: (x_koord, y_koord), bsp: (1.2, 4.5)
// POST: Speichert den Vekotor in der Referenz.
std::istream& operator>>(std::istream& in, Vec2D& v);

// PRE: Gueltiger Vec2D
// POST: Gibt den Vektor in der Obigen Form aus.
std::ostream& operator<<(std::ostream& out, Vec2D v);

// PRE: Gueltige Queue
std::ostream& operator<<(std::ostream& out, Queue q);


