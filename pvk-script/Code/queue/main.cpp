#include "queue.h"

int main(){
  Queue q1 = Queue("Queue Nr. 1");
  Queue* q2 = new Queue("Queue Nr. 2");
  
  Vec2D v = {1 , 2};
  q1.enqueue(v);
  q1.enqueue({4, 8});
  
  q2->enqueue(Vec2D{3, 4});
  q2->enqueue(v);
  q2->enqueue(Vec2D{2, 5});
  
  Queue q3 = q1 + *q2;
  
  std::cout << q1;
  std::cout << *q2;
  std::cout << q3;
  
  q3 += *q2;
  std::cout << q3;

  *q2 = q3;

  std::cout << *q2;
}
