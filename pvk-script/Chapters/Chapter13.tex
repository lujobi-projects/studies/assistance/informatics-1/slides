% Chapter 1

% Main chapter title

% For referencing the chapter elsewhere, use \ref{Chapter1} 

%----------------------------------------------------------------------------------------

\section{Theorie}
Anders als beispielsweise Java, verfügt \cpp nicht über eine sogenannte Garbage-Collection. Das heisst, dass \cpp nicht selbst Variablen löscht, \textbf{die mit \luziemphcode{new} alloziert wurden}, wenn sie nicht mehr gebraucht werden. 
Um zu verhindern, dass unser Programm mehr und mehr Arbeitsspeicher auffrisst, müssen wir (von Hand!!!) alle dynamisch allozierten Variablen löschen, die nicht mehr gebraucht werden.

\begin{block}{Löschen von Referenzen und Variable}
\cpp erkennt von selbst, wenn Referenzen und \textbf{nicht-dynamisch allozierte} Variablen nicht mehr gebraucht werden und gibt deren Speicherplatz automatisch wieder frei.
\end{block}

\subsection{delete - Operator}
Soll ein Objekt gelöscht werden, von welchem wir vorher mit \luziemphcode{new} den dazugehörigen Pointer erhalten haben, geschieht dies folgendermassen:

\begin{lstlisting}
int main(){
	Typ* ptr = new Typ(wert, ...)
	delete ptr; // Speicherplatz wieder frei
}
\end{lstlisting}

\ifexamples
\subsubsection{Beispiel}
\begin{lstlisting}
int main(){
	Vec2D* v = new Vec2D(1, 2)
	delete v; // Speicherplatz wieder frei
}
\end{lstlisting}
\fi

\begin{alertblock}{Löschen von dynamisch allozierten Arrays}
Soll ein Array gelöscht werden, kann dieses mit dem delete-Array-Operator (\luziemphcode{delete [] ptr}) gelöscht werden. Der normale \luziemphcode{delete} -Operator würde nur das erste Element löschen.

\ifexamples
Beispiel:
\begin{lstlisting}
int main(){
	int* a = new int[10] {1, 2, 3};
	delete[] a; // Loescht ganzes Array
	delete a; // Wuerde nur erstes Element loeschen.
}\end{lstlisting}
\fi
\end{alertblock}

\subsection{Destruktor}
Immer wenn wir \luziemphcode{delete} für einen Pointer, der auf ein Objekt zeigt, aufrufen, wird auf Seite des betroffenen Objekts der Destruktor aufgerufen. Solange die Klasse keine dynamisch allozierten Variabeln enthält, ist alles in Ordnung, wenn der Destruktor nicht definiert ist. Enthält die Klasse allerdings solche Variabeln, würde das Objekt gelöscht, ohne alle enthaltenen Variabeln zu löschen. Der Destruktor sieht folgendermassen aus:

\begin{lstlisting}
class Class_name{
	// Deklarationen
	public:
	~Class_name(){
		//loesche alle dynamisch allozierten Variabeln
	}
}
\end{lstlisting}

\ifexamples
\subsubsection{Beispiel}
Der Stack aus Abschnitt \ref{stack} hat keinen Destruktor. Löschen wir den Stack, so bleiben alle Nodes des Stacks im Arbeitsspeicher, wir haben keine Ahnung wo, und können sie so nicht mehr finden. Ein Speicherleck entsteht. So könnte es zu einem Problem kommen:

\begin{lstlisting}
#include<stack.h>

int main(){
	Stack * s1 = new Stack();
	s1->push("Stuff");
	s1->push("more Stuff");
	
	delete s1; // Speicherleck entsteht, "Stuff" und "more Stuff" bleibt im Arbeitsspeicher
}\end{lstlisting}

So könnte der Destruktor für Stack aussehen:\\
Im stack.h File:
\begin{lstlisting}
// im private Bereich:
void deleteNode(Node* node);
// im public Bereich:
~Stack();\end{lstlisting}

Im stack.cpp File:
\begin{lstlisting}
//loesche rekursiv alle Nodes unterhalb des uebergebenen nodes + den uebergebenen.
void Stack::deleteNode(Node* node){
	if (n->below != nullptr){
		deleteNode(node->below); // Gehe weiter den Stack runter, bis letzter Node erreicht.
	}
	delete node; // Loesche den aktuell untersten Node.
}

Stack::~Stack(){
	deleteNode(top_node); // Loesche alle Nodes bis und mit top_node
}\end{lstlisting}
\fi

\subsection{Copy-Konstruktor}
Der Copy-Konstruktor wird aufgerufen, wenn...
\begin{enumerate}
	\item ... ein Objekt \textbf{by value} an eine Funktion übergeben wird.
	\item ... ein Objekt \textbf{by value} von einer Funktion zurückgegeben wird.
	\item ... ein neues Objekt auf Basis eines alten Objektes initialisiert wird.
	\item ... der Compiler ein temporäres Objekt erzeugt.
\end{enumerate}
Den Copy-Konstruktor werde ich anhand des dritten Punktes besprechen, allerdings sind für uns auch Punkt 1 und 2 wichtig.
\begin{alertblock}{Achtung}
Der Copy-Konstruktor wird nur im folgenden Fall aufgerufen:
\begin{lstlisting}
Complex c1(1, 2);
Complex c2(2, 3);

Compiler c3 = c1; // Copy-Konstruktor
c2 = c1; // KEIN Copy-Konstruktor dafuer Zuweisung\end{lstlisting}
\end{alertblock}

Per default kopiert \cpp alle Werte der Membervariablen an die neue Stelle. Haben wir allerdings dynamisch allozierte Variablen, werden auch von den Pointern, die darauf zeigen, nur die Adressen kopiert. Wie in Abbildung \ref{fig:copy-constructor} illustriert, führt dies zu einem Problem, da die Nodes nicht kopiert wurden.\\
\begin{figure}[!htpb]
	\centering
	\includegraphics[width=0.7\linewidth]{Figures/copy-constructor}
	\caption{Problem ohne Copy-Konstruktor}
	\label{fig:copy-constructor}
\end{figure}
\\\\Der Copy-Konstruktor  sieht folgendermassen aus:
\begin{lstlisting}
class Class_name{
	// Deklarationen
	public:
	Class_name(const Class_name& other){
		// kopiere alle dynamisch allozierten Variabeln
	}
}\end{lstlisting}

\ifexamples
\subsubsection{Beispiel}
Der Stack aus Abschnitt \ref{stack} hat keinen Copy-Konstruktor. Kopieren wir den Stack, so würden jetzt zwei Stacks auf dieselben Nodes zeigen. So könnte der Copy-Konstruktor für Stack aussehen:

Im stack.h File:
\begin{lstlisting}
// im public Bereich:
Stack(const Stack& other);\end{lstlisting}

Im stack.cpp File:
\begin{lstlisting}
Stack::Stack(const Stack& other){
	if (other.top_node == nullptr){ // falls Stack leer waere
		this->top_node = nullptr;
		return;
	} else {
		this->top_node = new {other.top_node->data, nullptr}; // top_node extra behandeln.
		Node* current_other = other.top_node->below;
		Node* current_this = this->top_node;
		
		while(current_other != nullptr){
			Node* temp = new {current_other->value, nullptr}; // Neuen Node mit bekanntem Wert
			current_this->next = temp; // Beim aelteren neuen Node muss das next gesetzt werden
			
			current_other = current_other->below; // Springe einen Node weiter
			current_this = temp;
		}
	}
}\end{lstlisting}
\fi

\subsection{Zuweisungsoperator}
Der Zuweisungsoperator ist quasi eine Mischung aus Copy-Konstruktor und Destruktor. Wo er auftritt, haben wir schon gesehen:
\begin{lstlisting}
Complex c1(1, 2);
Complex c2(2, 3);

Complex c3 = c1; // Copy-Konstruktor
c2 = c1; // Zuweisungsoperator\end{lstlisting}

\ifexamples
\subsubsection{Beispiel}
Hier wollen wir tatsächlich die Pointer kopieren, allerdings müssen wir die alten Nodes löschen. Wir müssen den Zuweisungsoperator überladen.

So könnte der Zuweisungsoperator für den Stack überladen werden:

Im stack.h File:
\begin{lstlisting}
// im private Bereich:
void deleteNode(Node* node); // gesehen beim Destruktor
// im public Bereich:
Stack& operator= (const Stack& s)\end{lstlisting}

Im stack.cpp File:
\begin{lstlisting}
Stack& stack::operator= (const Stack& s){
	deleteNode(this->top_node); // loesche alle alten Nodes
	top_node = s.top_node; // setze top_node auf Node von anderem Stack
	return s;
}\end{lstlisting}
\fi
