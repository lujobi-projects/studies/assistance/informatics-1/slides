
%----------------------------------------------------------------------------------------


\section{Theorie}
Um Zahlen darzustellen, müssen wir uns auf ein System einigen. Im Alltag hat sich das Dezimalsystem durchgesetzt. Für Computer sind allerdings andere Systeme von Vorteil, besonders erwähnt sei hier das Binärsystem. Dieses wird manchmal zur besseren Lesbarkeit in ein Hexadezimal- oder Oktalsystem übersetzt. Im ersten Teil wird auf Ganzzahlen und im zweiten Teil auf Fliesskommazahlen eingegangen. Um verschiedene Systeme klar zu kennzeichnen, wird ein Subskript in runden Klammern verwendet (Für dezimal: $[123]_{(10)}$).
\subsection{Dezimal- und Binärsystem}
\label{bindez}
Die Darstellung von \textbf{Ganzzahlen} erfordert keine besonderen Strukturen. Die Idee ist, dass sich alle Zahlen im Wertebereich des Systems darstellen lassen. Am Beispiel des Dezimalsystems erklärt: Zur Darstellung haben wir 10 Zahlen zur Verfügung $[0, 1, ..., 9]$. Sobald die erste Stelle "{}aufgebraucht"{} ist, wird eine neue Stelle angefügt welche die Zahlen multipliziert mit 10 darstellen. Das geht für alle Stellen so weiter... In der Informatik hat sich das Binärsystem durchgesetzt, da dies nur aus Einsen und Nullen besteht und sich somit mit Strom einfach darstellen lässt. In den folgenden Abschnitten wird auf die Algorithmen zur Umwandlung von Dezimal- zu Binärsystem und vice-versa eingegangen. 
\subsubsection{Binär zu Dezimal}
Grundsätzlich ändert sich wenig verglichen mit der Beschreibung des Dezimalsystems, ausser dass wir jetzt den Faktor 2 statt 10 zwischen den Stellen haben.
\begin{block}{Algorithmus zur Konversion von Binär zu Dezimal}
	Gegeben sei die Zahl $b_nb_{n-1}b_{n-2}\dots b_2b_1b_0$ in \textbf{Binärschreibweise} d.h $\forall b_x \in [0, 1]$. Die dazugehörige Dezimalzahl erhalten wir mit dieser Formel:
	\begin{align}
	[n]_{(10)} = \sum_{i = 0}^{n} b_i \cdot 2^i
	\end{align}
\end{block}
\ifexamples
\paragraph{Beispiel}
\begin{align}
[1011]_{2} & = 1\cdot 2^3 + 0\cdot 2^2 +1\cdot 2^1 +1\cdot 2^0 = 8 + 2 + 1 = 11 \\
[100101]_{2} & = 1\cdot 2^5 + 0\cdot 2^4 +0\cdot 2^3 + 1\cdot 2^2 +0\cdot 2^1 +1\cdot 2^0 \\&= 32 + 0 + 0+ 4 + 0 +1 = 37
\end{align}
\fi

\subsubsection{Dezimal zu Binär}
Hier muss die Stelle von Dezimal auf Binär umgewandelt werden. Dazu ist folgender Algorithmus zu verwenden.
\begin{block}{Algorithmus zur Konversion von Dezimal zu Binär}
	Gegeben sei die Zahl $d = d_nd_{n-1}d_{n-2}\dots d_2d_1d_0$ in \textbf{Dezimalschreibweise} d.h $\forall d_x \in [0, 1, ..., 9]$. Die dazugehörige Binärzahl erhalten wir mit diesem Algorithmus:
	\begin{enumerate}
		\item rechne $d$ modulo $2 \rightarrow$ hinterste Stelle der Binärzahl
		\item dividiere $d$ durch 2 \textbf{ohne Nachkommastellen} zu berücksichtigen 
		\item zurück zu 1)
	\end{enumerate}
\end{block}
\ifexamples
\paragraph{Beispiel}
Gegeben sei die Zahl $[109]_{10}$
\begin{align}
109 / 2 &=    54 * 2 + 1\\ 
54 / 2 &=    27 * 2 + 0\\
27 / 2 &=    13 * 2 + 1\\
13 / 2 &=    6 * 2 + 1\\
6 / 2 &=    3 * 2 + 0\\
3 / 2 &=    1 * 2 + 1\\
1 / 2 &=    0 * 2 + 1
\end{align}
$\rightarrow$ Die Binärdarstellung von $109$ ist $1101101$
\fi

\subsection{Negative Ganzzahlen}
\textbf{Dieser Teil ist so wahrscheinlich nicht direkt Teil der Prüfung, allerdings hilft er beim Verständnis, warum es bei der Konvertierung von unsigned zu signed int  (Siehe Abschnitt \ref{castingunint}) zu Problemen kommen kann.}
Sollen auch negative Ganzzahlen dargestellt werden, wird das erste Bit als Minus verwendet. D.h. wird das vorderste Bit gesetzt, ist die Zahl negativ. Allerdings bedeuten ab hier die anderen Bits, wieviel der kleinstmöglichen negativen Zahl wieder hinzu gerechnet wird.\\
\begin{block}{Negative Zahlen lesen}
	Gegeben sei die Zahl $b_nb_{n-1}b_{n-2}\dots b_2b_1b_0$ in \textbf{Binärschreibweise} d.h $\forall x \neq n \ b_x \in [0, 1]$. Wir haben ein n-Bit-System und wissen, dass wir negative Zahlen darstellen wollen, das vorderste Bit ist eine Eins. Wir verwenden folgenden Algorithmus:
	\begin{enumerate}
		\item Berechne $-2^{n-1}$
		\item Setze $b_n = 0$, berechne die Binärzahl wie oben erwähnt
		\item Lösung ist Resultat von 1) plus Resultat von 2)
	\end{enumerate}
\end{block}

\ifexamples
\paragraph{Beispiel}
\begin{center}
	\begin{tabular}[c]{r|r|r}
		bin&uint&int\\
		\hline
		0000&0&0\\
		0010&1&1\\
		0010&2&2\\
		0011&3&3\\
		0100&4&4\\
		0101&5&5\\
		0110&6&6\\
		0111&7&7\\
		1000&8&-8\\
		1001&9&-7\\
		1010&10&-6\\
		1011&11&-5\\
		1100&12&-4\\
		1101&13&-3\\
		1110&14&-2\\
		1111&15&-1\\
	\end{tabular}
\end{center}
\fi

\subsection{Andere Zahlensysteme}
Grundsätzlich können wir die Algorithmen aus Abschnitt \ref{bindez} problemlos auf jedes andere Zahlensystem anpassen. Sind die Konvertierungen etwas mühsamer (z.B. Tertiär zu Hexadezimalsystem), empfiehlt es sich, immer den Schritt via das (uns wohlbekannte) Dezimalsystem zu machen.
\begin{block}{Algorithmus zur Konversion zu Dezimal}
	Gegeben sei die Zahl $z_nz_{n-1}z_{n-2}\dots z_2z_1z_0$ in einem x-System d.h $\forall z_x \in [0, x-1]$. Die dazugehörige Dezimalzahl erhalten wir mit dieser Formel:
	\begin{align}
	[n]_{(10)} = \sum_{i = 0}^{n} b_i \cdot x^i
	\end{align}
\end{block}

\begin{block}{Algorithmus zur Konversion von Dezimal}
	Gegeben sei die Zahl $d = d_nd_{n-1}d_{n-2}\dots d_2d_1d_0$ in \textbf{Dezimalschreibweise} d.h $\forall d_x \in [0, 1, ..., 9]$. Die dazugehörige Darstellung im x-System erhalten wir mit diesem Algorithmus:
	\begin{enumerate}
		\item rechne $d$ modulo $x \rightarrow$ hinterste Stelle der Binärzahl
		\item dividiere $d$ durch $x$ \textbf{ohne Nachkommastellen} zu berücksichtigen 
		\item zurück zu 1)
	\end{enumerate}
\end{block}

\ifexamples
\paragraph{Beispiele}
\begin{align}
	[1021]_{(3)} &= 1 \cdot 3^3 + 0 \cdot 3^2 + 2 \cdot 3^1 + 1 \cdot 3^0 = 27 + 0 + 6 + 1 = 34\\
	[107]_{(10) } &= 15 + 2/7\\
	&= 2 + 1/(7\cdot 7) + 2/7 
	&= [212]_{(7)}
\end{align}
\fi

\subsubsection{Binär-, Quartal- , Oktal-, und Hexadezimalsystem}
Da diese Systeme alle ein Vielfaches der Basis 2 sind, lassen sich die Systeme problemlos ineinander überführen. Da wir nur 10 Ziffern haben, werden beim Hexadezimalsystem Buchstaben ($[0, 1, ..., 9, a, b, c, d, e, f]$) zu Hilfe genommen. 
\begin{alertblock}{Achtung!}
	\luziemphcode{0b1100110} kennzeichnet eine Binärzahl in \cpp, \luziemphcode{0xf34de} eine Hexadezimalzahl.
\end{alertblock}
\begin{block}{2er Basis bei Zahlensystemen}
	Grundsätzlich ist es immer empfohlen den Umweg via dem Binärsystem zu mache, ist aber auch nicht zwingend nötig. Da alle Systeme dieselbe Basis haben, reicht es wenn wir die Blöcke jeweils einzeln in das gewünschte System übersetzen. Beim Hexadezimalsystem werden jeweils 4 Bit zu einem neuen Zeichen zusammengefasst, beim Oktalsystem 3 und beim Quartalsystem 2.
\end{block}
\ifexamples
\paragraph{Beispiel}
\begin{align}
[10111101001011]_{(2)} &= [0010\mid 1111\mid 0100 \mid 1011]_{(2)} \\&= [2\mid\text{f}\mid 8 \mid \text{b}]_{(16)}  = [2\text{f} 8  \text{b}]_{(16)}\\
[10111101001011]_{(2)} &= [010\mid 101\mid 101\mid 001\mid 011]_{(2)} \\&= [2\mid 5\mid 5\mid 1\mid 3\mid ]_{(8)} = [25513]_{(8)}\\
[1c9]_{(16)} &= [0001\mid 1100\mid 1001]_{2} = [111001001]_2
\end{align}
\fi

\subsection{Fliesskommazahlen}
Um Kommazahlen darzustellen, bedient sich \cpp dem Konzept der Fliesskommazahlen. Diese sind uns etwa aus der wissenschaftlichen Schreibweise bekannt. Wir haben eine Kommazahl mit einer Ziffer vor dem Komma und anschliessend einen Faktor (in Potenzschreibweise), mit dem die Zahl multipliziert werden muss. Allgemein formuliert sieht dies folgendermassen aus:
\begin{centering}
	$\mathcal{F}(\beta, p, e_{min}, e_{max})$
\end{centering}
\begin{itemize}
	\item $\beta \ge 2$ Basis: z. B. 2 für binär, 10 für dezimal 
	\item $p$ Präzision, Anzahl Kommastellen (mit der Zahl vor dem Komma)
	\item $e_{min}$ Minimaler Exponent
	\item $e_{max}$ Maximaler Exponent
\end{itemize}
$\mathcal{F}$ stellt dabei die folgende Zahl dar: \\ 
\begin{centering}
	\begin{center}
		$\pm b_0.b_1b_2\dotsb_{p-1}\cdot b^e$\\
		wobei $b_i \in \{0, ..., b-1\}$ und $b_0 \neq 0$, und
		$e\in\{e_{min}, e_{min} + 1, \ ..., \ e_{max} - 1, e_{max} \}$.
	\end{center}
\end{centering}
Bei Fliesskommazahlen nehmen wir bewusst in Kauf, dass gewisse Zahlen nicht mehr zu 100\% korrekt dargestellt werden. Dafür wird unser Wertebereich massiv vergrössert.

\subsubsection{Binäre Fliesskommazahlen}
Diese sind für uns besonders wichtig, da Computer im Binärsystem rechnen. Fliesskommazahlen im Binärsystem bedeuten: $\mathcal{F}(2, p, e_{min}, e_{max})$ 

Für einen \luziemphcode{float} haben wir 32 Bit zur Verfügung, für einen \luziemphcode{double} sind es 64 Bit, diese werden durch den \cpp-Standart folgendermassen verteilt:
\begin{itemize}
	\item \luziemphcode{float }	$\mathcal{F_{\text{32}}^*}(2, 24, -126, 127)$
	\item\luziemphcode{double }  $\mathcal{F_{\text{64}}^*}(2, 52, -1022, 1023)$
\end{itemize}
\paragraph{Probleme mit 0, und negativen Zahlen}
Da immer eine  Zahl ungleich Null vor dem Komma stehen muss, können wir \underline{0 nie exakt darstellen}. Deshalb werden die Exponenten -127 und -128 bzw. -1023 und -1024 zur Darstellung von $0, \infty, -\infty, NaN$ verwendet.
\paragraph{Rechnen mit Fliesskommazahlen}
Vor allem Addition und Subtraktion bedürfen spezieller Betrachtung wenn es ums Rechnen mit Fliesskommazahlen geht.
\begin{block}{Addition und Subtraktion zweier Fliesskommazahlen}
\begin{enumerate}
	\item Beide Zahlen auf denselben Exponenten bringen
	\item Operation durchführen
	\item Ergebnis normalisieren
	\item Wenn nötig: Runden
\end{enumerate}
\end{block}

\ifexamples
\subparagraph{Beispiel}
Unter der Verwendung dieses Floating-Point-Systems (
$\mathcal{F^*}(2, 3, -4, 4)$) sind die Additionen  $(10 + 0.5) + 0.5$ und  $(0.5 + 0.5) + 10$ \textbf{im gegebenen System} zu berechnen.
\begin{table}[!htbp]
	\centering
	\begin{tabular}{rll||rll}
		& dezimal  & binär & & dezimal  & binär \\\hline\hline
		&$10$ & $1.01\cdot2^3$&	&$0.5$ & $1.0\cdot2^{-1}$ \\
		$+$ & $0.5$ &$0.0001\cdot2^3$& $+$ & $0.5$ &   $1.0\cdot2^{-1}$ \\ \hline
		$=$ &  &$1.0101\cdot2^3$&$=$ & &$1.0\cdot2^{0}$   \\
		Runden & &$1.01\cdot2^3$& & &$1.0\cdot2^{0}$\\
		$+$ & $0.5$ &$0.0001\cdot2^3$&$+$ & $10$ &  $1010.0\cdot2^{0}$\\ \hline
		$=$ &  &$1.0101\cdot2^3$& &&  $1011.0\cdot2^{0}$ \\
		Runden &$10 \leftarrow $&$1.01\cdot2^3$&   &$12 \leftarrow $& $1.10\cdot2^{3}$ \\
	\end{tabular}
\end{table}
\fi


\ifexamples
Folgendes Beispiel zeigt ein Kommasystem, welches nicht normalisiert wurde.
\begin{table}[!htbp]
	\centering
	\begin{tabular}{l|c|c|c|c|c|c|c|c}
		binär: & $1$ & $1$ & $1$ & $1$ & $.$ & $1$ & $1$ & $1$ \\ \hline
		dezimal: & $8$ & $4$ & $2$ & $1$ &  & $\frac{1}{2}$ & $\frac{1}{4}$ & $\frac{1}{8}$ \\ 
	\end{tabular}
\end{table}
Normalisiert heisst, dass \textbf{genau} eine Ziffer vor dem Komma steht, der Rest wird über den Exponenten geregelt. \textbf{"{}Unsere"{} Fliesskommasysteme müssen immer normalisiert werden!}
\fi

\subsubsection{Konversion von Dezimal Kommazahlen zu Binär}
Um eine dezimale Kommazahl in eine binäre Fliesskommazahl zu konvertieren, verwenden wir folgenden Algorithmus:
\begin{block}{Dezimal Komma zu Binär Komma}
	Der folgende Teil gilt nur für Zahlen kleiner als 2, alles grössere kann gemäss Abschnitt \ref{bindez} umgerechnet werden. Wir beginnen mit $ i = 0$;
	\begin{itemize}
		\item $b_i = \begin{cases}
		1   & \text{if }x_{1} \ge 1\\
		0 & \text{else}
		\end{cases}$
		\item $x_{i+1} = 2 (x_i-b_i)$
		\item fahre solange fort, bis sich $x$ wiederholt oder $x = 0$
	\end{itemize}
	$z = b_0.b_1b_2...$
\end{block}
		
\ifexamples
\paragraph{Beispiel}
Umwandlung von $[1.9]_{10}$ in binäre Kommadarstellung.\\
\begin{table}[!htbp]
	\centering
	\begin{tabular}{c||c|c|c}
		i & $x_i$ & $b_i$ &$x_i-b_i$ \\ \hline \hline
		1 & 1.9 & 1 & 0.9 \\ \hline
		2 & 1.8 & 1 & 0.8\\ \hline
		3 & \textbf{1.6 }& 1 & 0.6 \\ \hline
		4 & 1.2 & 1 & 0.2 \\ \hline
		5 & 0.4 & 0 & 0.4 \\ \hline
		6 & 0.8 & 0 & 0.8 \\ \hline
		7 & \text{1.6} & 1 & 0.6 \\
	\end{tabular}
\end{table}

Beachte:  1.6 kommt ein zweites Mal vor $\rightarrow$ ab da wiederholt sich die binäre Darstellung: \\
$1.9_{10} = 1.1\overline{1100}_2$
\fi

\subsubsection{Andere Fliesskommasysteme}
Wie auch bei Ganzzahlen, können weitere Fliesskommasysteme gebildet werden, indem die Basis jeweils angepasst wird.
