#!/bin/sh

rm -f ../*.aux
rm -f ../*.out
rm -f ../*.run.xml
rm -f ../*.log
rm -f ../*.lot
rm -f ../*.bcf
rm -f ../*.bib.bbl
rm -f ../*.bib.blg
rm -f ../*.lof
rm -f ../*.tex.bbl
rm -f ../*.tex.blg
rm -f ../*.toc
rm -f ../*.blg
rm -f ../*/*.aux
rm -f ../*.synctex.g
rm -f ../*.synctex.gz
rm -f ../*.bbl
