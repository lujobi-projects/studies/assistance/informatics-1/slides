#include <iostream>
int main () {
	// input
	unsigned int strangesum = 0;
	unsigned int n;
	std::cin >> n;
	// computation
	for (unsigned int i = 1; i <= n; i++)
	if (i%2 == 1)
		if (i%5 != 0)
			strangesum += i;
	// output
	std::cout << "The strange sum is " << strangesum << ".\n";
	return 0;
}