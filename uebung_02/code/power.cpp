#include <iostream>
#include <cassert>

int main () {
  unsigned int n;
  std::cin >> n;
  assert(n >= 1);
  
  unsigned int power = 1;
  for (; power <= n / 2; power *= 2);
  
  std::cout << power << std::endl;
  
  return 0;
}
