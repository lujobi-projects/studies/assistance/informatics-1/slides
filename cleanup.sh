#!/bin/bash

base_dir=ueb*

endings=(.aux .log .nav .out .snm .toc .synctex.gz .vrb .fls .fdb_latexmk .pdf)

for i in ${endings[@]} ; do
  find . -name ${i} -type f -delete
  echo ${i}
done

rm uebung_*/images/title.png

exit 0