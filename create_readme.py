import mdutils
import config as config

def table_from_array(mdFile, array):

  if array is not None:
    title=array[0]
    table = list(title.values())

    for row in array[1:]:
      new_row = []
      for item in title.keys():
        if row[item] is None:
          new_row.append('')
        elif isinstance(row[item], dict):
          string = ''
          if 'link' in row[item]:
            string = '[' + row[item]['text'] + '](' + row[item]['link'] + ')'
          new_row.append(string)
        else:
          new_row.append(row[item])      
      table.extend(new_row)
    mdFile.new_table(columns=len(array[0]), rows=len(array), text=table, text_align='center')

  

def create_readme():
  mdFile = mdutils.MdUtils(file_name='README')
  
  mdFile.new_header(level=1, title='Informatik Übung')

  mdFile.new_paragraph('In diesem Verzeichnis findet Ihr die neusten Versionen meiner Übungsmaterialien sowie einige weitere Links\n')
  mdFile.new_paragraph('Meine Übungsstunde findet jeweils am **Mittwoch 13.00-15.00** auf **[Zoom](https://ethz.zoom.us/j/326682521)** statt\n')
  mdFile.new_paragraph('Für den korrekten Inhalt der Unterlagen bin ich stets bemüht, allerdings kann ich keine Garantie geben. Solltet Ihr Fehler entdecken bitte ich euch mir diese unter [luzibier@ethz.ch](mailto:luzibier@ethz.ch) zu melden.')
  mdFile.new_paragraph("Link zur Mailingliste: [Link](https://forms.gle/PcpKk1qqi9xFAXdE9)")

  # mdFile.new_header(level=, title='Unterlagen')
  mdFile.new_header(level=2, title='Unterlagen')

  mdFile.new_line()
  table_from_array(mdFile, config.exercises())
  
  mdFile.new_header(level=3, title='PVK')
  mdFile.new_line()
  table_from_array(mdFile, config.pvk)
  mdFile.new_paragraph("Slides zum PVK: [slides.com](https://slides.com/luzianbieri/informatik-i-pvk-2020)")

  mdFile.new_header(level=3, title='Zusammenfassungen')
  mdFile.new_line()
  table_from_array(mdFile, config.summary)


  mdFile.create_md_file()

if __name__ == '__main__':
  create_readme()