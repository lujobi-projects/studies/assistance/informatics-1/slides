



# Informatik Übung


In diesem Verzeichnis findet Ihr die neusten Versionen meiner Übungsmaterialien sowie einige weitere Links


Meine Übungsstunde findet jeweils am **Mittwoch 13.00-15.00** auf **[Zoom](https://ethz.zoom.us/j/326682521)** statt


Für den korrekten Inhalt der Unterlagen bin ich stets bemüht, allerdings kann ich keine Garantie geben. Solltet Ihr 
Fehler entdecken bitte ich euch mir diese unter [luzibier@ethz.ch](mailto:luzibier@ethz.ch) zu melden.

Link zur Mailingliste: [Link](https://forms.gle/PcpKk1qqi9xFAXdE9)
## Unterlagen
  

|Titel|Folien|Handout|Zusatz|
| :---: | :---: | :---: | :---: |
|Übung 1|[slides_01.pdf](./out/slides_01.pdf)|[handout_01.pdf](./out/handout_01.pdf)|[add_01.pdf](./out/add_01.pdf)|
|Übung 2|[slides_02.pdf](./out/slides_02.pdf)|[handout_02.pdf](./out/handout_02.pdf)||
|Übung 3|[slides_03.pdf](./out/slides_03.pdf)|[handout_03.pdf](./out/handout_03.pdf)|[add_03.pdf](./out/add_03.pdf)|
|Übung 4|[slides_04.pdf](./out/slides_04.pdf)|[handout_04.pdf](./out/handout_04.pdf)|[add_04.pdf](./out/add_04.pdf)|
|Übung 5|[slides_05.pdf](./out/slides_05.pdf)|[handout_05.pdf](./out/handout_05.pdf)||
|Übung 6|[slides_06.pdf](./out/slides_06.pdf)|[handout_06.pdf](./out/handout_06.pdf)|[add_06.pdf](./out/add_06.pdf)|
|Übung 7|[slides_07.pdf](./out/slides_07.pdf)|[handout_07.pdf](./out/handout_07.pdf)|[hanoi.pdf](https://polybox.ethz.ch/index.php/s/kzVlFnVhKylBLOY)|
|Übung 8|[slides_08.pdf](./out/slides_08.pdf)|[handout_08.pdf](./out/handout_08.pdf)|[ebnf.txt](https://polybox.ethz.ch/index.php/s/LoZS7pjEuHbwFQO)|
|Übung 9|[slides_09.pdf](./out/slides_09.pdf)|[handout_09.pdf](./out/handout_09.pdf)|[infix.cpp](https://polybox.ethz.ch/index.php/s/61nBTgruPcPmmLG)|
|Übung 10|[slides_10.pdf](./out/slides_10.pdf)|[handout_10.pdf](./out/handout_10.pdf)||
|Übung 11|[slides_11.pdf](./out/slides_11.pdf)|[handout_11.pdf](./out/handout_11.pdf)||
|Übung 12|[slides_12.pdf](./out/slides_12.pdf)|[handout_12.pdf](./out/handout_12.pdf)||

### PVK
  

|Titel|PDF|
| :---: | :---: |
|PVK-Skript komplett|[pvk_info_komplett.pdf](./out/pvk_info_komplett.pdf)|
|PVK-Skript Theorie|[pvk_info_theorie.pdf](./out/pvk_info_theorie.pdf)|
|PVK-Skript Theorie ohne Beispiele|[pvk_info_theorie_ohne_beispiele.pdf](./out/pvk_info_theorie_ohne_beispiele.pdf)|
|PVK-Skript Aufgaben|[pvk_info_aufgaben.pdf](./out/pvk_info_aufgaben.pdf)|
|PVK-Skript Aufgaben Druckversion|[pvk_info_aufgaben_druckbar.pdf](./out/pvk_info_aufgaben_druckbar.pdf)|
|||
|EBNF_cookbook|[EBNF_cookbook.md](https://gitlab.ethz.ch/luzibier/informatik-slides/-/blob/master/additional_pdfs/EBNF_cookbook.md)|


Slides zum PVK: [slides.com](https://slides.com/luzianbieri/informatik-i-pvk-2020)
### Zusammenfassungen
  

|Titel|PDF|docx|
| :---: | :---: | :---: |
|Meine Zusammenfassung|[ZF_info.pdf](https://amiv.ethz.ch/de/studydocuments/5cf433b9b0f70bbaa1ffbace)|[ZF_info.docx](https://gitlab.ethz.ch/luzibier/informatik-slides/blob/master/Zusammenfassung_Info_FS_2019.docx)|
