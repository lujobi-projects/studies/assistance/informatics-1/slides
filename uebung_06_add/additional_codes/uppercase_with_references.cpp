#include <iostream>
#include <vector>
#include <ios>

// POST: Gibt den zugehörigen Grossbuchstaben zurück
// Übergabe des chars hier als Referenz, da so der betroffene  Buchstabe direkt beeinflusst wird. 
void char_to_upper(char& letter) {
  // Nur für kleine Buchstaben (Sonderzeichen bleiben wie sie sind!!!)
  if ('a' <= letter && letter <= 'z') {
    letter -= 'a' - 'A';  // Differenz zwischen Gross- und Kleinbuchstaben bleibt konstant, da sie im ASCII Format in einer Reihe sind
  }
}

// POST: Converts all letters to upper-case.
// Übergabe des Vektors als Referenz, dass hier der "betroffene" Vektro direkt bearbeitet wird.
void to_upper(std::vector<char>& letters) {
  for (unsigned int i = 0; i < letters.size(); ++i) {
    char_to_upper(letters[i]);
  }
}

int main () {
  //Einstellung von std::cin
  std::cin >> std::noskipws;
  
  std::vector<char> letters;
  char ch;
  
  //Einlesen der Chars
  do {
    std::cin >> ch;
    letters.push_back(ch);
  } while (ch != '\n');
  
  //In Vektor Letters  werden alle Klein- zu Grossbuchstaben gemacht.
  to_upper(letters);
  
  //Ausgabe der Uppercase-Letters
  for (unsigned int i = 0; i < letters.size(); ++i) {
    std::cout << letters[i];
  }
  return 0;
}
