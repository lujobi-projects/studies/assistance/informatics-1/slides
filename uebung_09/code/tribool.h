class tribool {
private:
  unsigned int value; // value = 0 -> false, = 1 -> unknown, = 2 -> true;
public:
  tribool(unsigned int val); // Contructor nimmt ein val element [0, 1, 2]

  bool operator== (const tribool& other) const; // tri1 == tri2 -> tri1 in class other wird übergeben

  tribool operator&&(const tribool& other) const; // tribool && tribool 
  tribool operator||(const tribool& other) const; // tribool || tribool 
  
  tribool operator&& (const bool& other) const; // tribool && bool
  tribool operator|| (const bool& other) const; // tribool || bool
  
};

tribool tribool_false(); //Gibt ein tribool mit dem Wert false zurück
tribool tribool_unknown(); //Gibt ein tribool mit dem Wert unknown zurück
tribool tribool_true(); //Gibt ein tribool mit dem Wert true zurück

tribool operator&& (const bool& first, const tribool& second); // bool && tribool
tribool operator|| (const bool& first, const tribool& second); // bool || tribool
std::istream& operator>> (std::istream& in, tribool& t);  // einlesen und in t speichern
std::ostream& operator<< (std::ostream& out, const tribool& t); // t in out ausgeben



