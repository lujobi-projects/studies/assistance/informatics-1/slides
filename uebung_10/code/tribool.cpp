#include <iostream>
#include "tribool.h"

tribool::tribool(unsigned int val) { // val wird übergeben
  value = val; // und die Membervariable wird gleich val gesetzt
}

bool tribool::operator== (const tribool &other) const {
  return value == other.value; //tri1 == tri2 -> value ist value von tri1 other.value ist value von tri2
}

tribool tribool::operator&& (const tribool &other) const {
  tribool result(std::min(value, other.value)); // ausnutzen dass value ein int ist
  return result; //tri1 && tri2 -> value ist value von tri1 other.value ist value von tri2
}

tribool tribool::operator|| (const tribool &other) const {
  tribool result(std::max(value, other.value));// ausnutzen dass value ein int ist
  return result;
}

tribool tribool::operator&& (const bool &other) const {
  unsigned int other_value = other * 2;// bool nach tribool konverieren
  tribool result(std::min(value, other_value));// ausnutzen dass value ein int ist
  return result;
}

tribool tribool::operator|| (const bool &other) const {
  unsigned int other_value = other * 2; // bool nach tribool konverieren
  tribool result(std::max(value, other_value)); // ausnutzen dass value ein int ist
  return result;
}

tribool operator&& (const bool& first, const tribool& second) {
  return second && first; // tribool && bool haben wir schon definiert
}

tribool operator|| (const bool& first, const tribool& second) {
  return second || first; // tribool || bool haben wir schon definiert
}

// tribool tri;
// std::cin >> tri;
std::istream& operator>> (std::istream &in, tribool& t) {
  std::string value;
  if (in >> value) { //string einlesen
    if (value == "true") {
      t = tribool_true(); //... und je nach dem t setzten
    } else if (value == "unknown") {
      t = tribool_unknown();
    } else if (value == "false") {
      t = tribool_false();
    } else {
      // Read unrecognised value.
      in.setstate(std::ios::failbit);
    }
  } else {
    // Failed to read.
    in.setstate(std::ios::failbit);
  }
  return in;
}

// tribool tri;
// std::cin << tri;
std::ostream& operator<< (std::ostream &out, const tribool& t) {
  if (t == tribool_false()) { //je nachdem welchen wert tribool hat
    std::cout << "false"; //... wird dies ausgegebne
  } else if (t == tribool_unknown()) {
    std::cout << "unknown";
  } else {
    std::cout << "true";
  }
  return out;
}

tribool tribool_false() {
  return tribool(0); // 0 heisst false
}

tribool tribool_unknown() {
  return tribool(1); // 1 heisst unknown
}

tribool tribool_true() {
  return tribool(2); // 2 heisst true
}
