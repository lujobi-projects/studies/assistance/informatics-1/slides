out_dir = './out'

current_ex = 12

add_list = [
    {'text': 'add_01.pdf', 'link': './out/add_01.pdf'},
    None,
    {'text': 'add_03.pdf', 'link': './out/add_03.pdf'},
    {'text': 'add_04.pdf', 'link': './out/add_04.pdf'},
    None,
    {'text': 'add_06.pdf', 'link': './out/add_06.pdf'},
    {'text': 'hanoi.pdf', 'link': 'https://polybox.ethz.ch/index.php/s/kzVlFnVhKylBLOY'},
    {'text': 'ebnf.txt', 'link': 'https://polybox.ethz.ch/index.php/s/LoZS7pjEuHbwFQO'},
    {'text': 'infix.cpp', 'link': 'https://polybox.ethz.ch/index.php/s/61nBTgruPcPmmLG'},
    None,
]

exercises_template = {
    'title': 'Titel',
    'slides': 'Folien',
    'handout': 'Handout',
    'add': 'Zusatz'
}


def nr2ex(i):
    if i == 0:
        return exercises_template
    i_str = str(i) if i >= 10 else '0' + str(i)
    return {
        'title': f'Übung {i}',
        'slides': {'text': f'slides_{i_str}.pdf', 'link': f'./out/slides_{i_str}.pdf'},
        'handout': {'text': f'handout_{i_str}.pdf', 'link': f'./out/handout_{i_str}.pdf'},
        'add': add_list[i-1] if len(add_list) > i else None
    }


def exercises():
    return list(map(lambda x: nr2ex(x), range(current_ex+1)))


pvk = [
    {
        'title': 'Titel',
        'page': 'PDF',
    },
    {
        'title': 'PVK-Skript komplett',
        'page': {'text': 'pvk_info_komplett.pdf', 'link': './out/pvk_info_komplett.pdf'},
    },
    {
        'title': 'PVK-Skript Theorie',
        'page': {'text': 'pvk_info_theorie.pdf', 'link': './out/pvk_info_theorie.pdf'},
    },
    {
        'title': 'PVK-Skript Theorie ohne Beispiele',
        'page': {'text': 'pvk_info_theorie_ohne_beispiele.pdf', 'link': './out/pvk_info_theorie_ohne_beispiele.pdf'},
    },
    {
        'title': 'PVK-Skript Aufgaben',
        'page': {'text': 'pvk_info_aufgaben.pdf', 'link': './out/pvk_info_aufgaben.pdf'},
    },
    {
        'title': 'PVK-Skript Aufgaben Druckversion',
        'page': {'text': 'pvk_info_aufgaben_druckbar.pdf', 'link': './out/pvk_info_aufgaben_druckbar.pdf'},
    },
    {
        'title': '', 'page': {'text': '', 'page': ''}},
    {
        'title': 'EBNF_cookbook',
        'page': {'text': 'EBNF_cookbook.md', 'link': 'https://gitlab.ethz.ch/luzibier/informatik-slides/-/blob/master/additional_pdfs/EBNF_cookbook.md'}
    }
]

summary = [
    {
        'title': 'Titel',
        'page': 'PDF',
        'add': 'docx',
    },
    {
        'title': 'Meine Zusammenfassung',
        'page': {'text': 'ZF_info.pdf', 'link': 'https://amiv.ethz.ch/de/studydocuments/5cf433b9b0f70bbaa1ffbace'},
        'add': {'text': 'ZF_info.docx', 'link': 'https://gitlab.ethz.ch/luzibier/informatik-slides/blob/master/Zusammenfassung_Info_FS_2019.docx'},
    },
]
