// UNCOMMENT THE NEXT LINE TO ENABLE THE TESTS WITHOUT SUBMITTING
#include<iostream>
#include<istream>
#include<sstream>

bool seq  (std::istream& is);
bool term (std::istream& is);

char lookahead (std::istream& is){
  is >> std::ws;         // skip whitespaces
  if (is.eof()) 
    return 0;            // end of stream
  else 
    return is.peek();    // next character in is
}
bool consume  (std::istream& is, char c){
  if (lookahead (is) == c) {
    is >> c;
    return true;
  } else
    return false;
}

bool seq(std::istream& is){
  bool te = term(is);
  if(is.eof()){
    return te;
  } else {
    return te && consume(is, '_') && seq(is) && is.eof();
  }
}

bool term(std::istream& is){
  if (consume(is, 'A')){
    while (consume(is, 'a'));
    return true;
  } else if (consume(is, 'a')){
    while (consume(is, 'a'));
    return true;
  } else{
    return false;
  }
}

void check(std::string s){
  std::stringstream input (s);
  if (seq(input)){
    std::cout << s << " is valid" << std::endl;
  } else {
    std::cout << s << " is invalid"  << std::endl;
  }
}

int main(){
  std::string input;
  std::cin >> input;
  check(input);
}

