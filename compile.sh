#!/bin/bash

NUMBER_OF_EXERCISES=${1:-12}
OUTPUT_DIR=${2:-"out"}
LOG_DIR=${3:-"log"}
CLEAR_BUILD_FILES=true
DELETE_LN=false

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

failed_files=()
not_found_files=()
successful_files=()

mkdir_new(){
    if ! test -d "$1"; then
        mkdir $1
    else 
        rm $1
        mkdir $1
    fi

}

compile_file() {
	dir=$1
    file=$2
    path="$dir/$file.tex"

    if test -f "$path" ; then
        cd $dir
        mkdir_new build

        ln ../beamer_files/beamercolorthemeprogressbar.sty beamercolorthemeprogressbar.sty 
        ln ../beamer_files/beamerfontthemeprogressbar.sty beamerfontthemeprogressbar.sty 
        ln ../beamer_files/beamerinnerthemeprogressbar.sty beamerinnerthemeprogressbar.sty 
        ln ../beamer_files/beamerouterthemeprogressbar.sty beamerouterthemeprogressbar.sty 
        ln ../beamer_files/beamerthemeprogressbar.sty beamerthemeprogressbar.sty 
        ln ../images/title.png images/title.png 

        for n in {1..4} ; do        
            # pdflatex -synctex=1 -interaction=nonstopmode -output-directory build "$file.tex"
            latexmk -bibtex -pdf -quiet "$file.tex"
        done

        if [ $? -gt 0 ] ; then
            cd ..
            failed_files+=( "$path" )
            return
        fi

        mv "$file.pdf" "../$OUTPUT_DIR"
        mv "$file.log" "../$LOG_DIR"

        if [ "$CLEAR_BUILD_FILES" = true ] ; then
            rm -rf build
            latexmk -c
        fi
        if [ "$DELETE_LN" = true ] ; then
            rm beamercolorthemeprogressbar.sty
            rm beamerfontthemeprogressbar.sty
            rm beamerinnerthemeprogressbar.sty
            rm beamerouterthemeprogressbar.sty
            rm beamerthemeprogressbar.sty
        fi

        successful_files+=("$file.tex   -->   $OUTPUT_DIR/$file.pdf")
        cd ..
    else
        not_found_files+=( "${path}" )
    fi

}

echo_array(){
    message=$1
    color=$2
    array=("${@:3}")

    echo -e "${color}${message}"

    if (("${#array[@]}" > 0 )) ; then
        for i in "${array[@]}" ; do
            echo $i
        done
    else
        echo "none"
    fi

    echo -e "${NC}"
}


mkdir_new $OUTPUT_DIR
mkdir_new $LOG_DIR

for i in $(seq 1 $NUMBER_OF_EXERCISES) ; do
	number="$i"
	if [ $i -le 9 ]; then
		number="0$number" 
	fi
	
	dir="uebung_$number"
	slide="slides_$number"
	handout="handout_$number"

	compile_file $dir $slide
	compile_file $dir $handout
    compile_file "${dir}_add" "add_$number"
done

compile_file "pvk-script" "pvk_info_aufgaben_druckbar"
compile_file "pvk-script" "pvk_info_aufgaben"
compile_file "pvk-script" "pvk_info_komplett"
compile_file "pvk-script" "pvk_info_theorie_ohne_beispiele"
compile_file "pvk-script" "pvk_info_theorie"

echo ""
echo_array "Successfully compiled files: " $GREEN "${successful_files[@]}"
echo ""
echo_array "Files which were not found: " $YELLOW "${not_found_files[@]}"
echo ""
echo_array "Files where compiling failed: " $RED "${failed_files[@]}"
