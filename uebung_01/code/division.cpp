#include <iostream>
int main() {
//input
	int a;
	std::cin >> a;
	int b;
	std::cin >> b;
//computation
	int whole = a / b;
	int remainder = a % b;
//output
	std::cout << a << " divided by " << b << " equals " << whole << " + " << remainder << "/" << b << ".\n";
	return 0;
}
