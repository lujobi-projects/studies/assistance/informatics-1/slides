#include <iostream>
#include <climits>


//Hier global defininert, da die Variablen erhalten bleiben müssen, bis das Programm beendet ist.
unsigned int fibonacci1 = 0, fibonacci2 = 1;

//*** Diese Funktion gibt Werte de Typs bool zurück, nimmt Zwei unsigned ints als Argumente
//True wenn die Zahl (number) restlos durch den divisor teilbar ist
bool is_divisible_by(unsigned int number, unsigned int divisor){
  return !(number%divisor);
}


//*** void heisst, dass es keinen Return-Value geben kann. ein leeres return beendet die Funktion
//Gibt die Position, die dazugehörige Zahl und die Info, ob Sie gerade ist, aus. 
void print_number_with_info(unsigned int position, unsigned int number){
  //Abbruch, wenn number == 0, da dann ein Fehler passiert sein muss. 
  if(number == 0){
    return;
  }
  std::cout << "The " << position;
  
  //fancy Variante eines if's (und vielen else ifs) (müsst ihr nicht können!)
  switch (position) {
    case 1:
      std::cout << "st";
      break;
    case 2:
      std::cout << "nd";
      break;
    case 3:
      std::cout << "rd";
      break;
    default:
      std::cout << "th";
  }
  
  std::cout << " Fibnacci Number: " << number;
  
  //Testet ob die Zahl gerade ist
  //%%% Aufruf von is_divisible_by
  if(is_divisible_by(number, 2)){
    std::cout << " is EVEN and";
  } else{
    std::cout << " is ODD and";
  }
  
  //Testet ob die Zahl durch 5 teilbar ist
  //%%% Aufruf von is_divisible_by
  if(!is_divisible_by(number, 5)){
    std::cout << " NOT";
  }
  
  std::cout << " DIVISIBLE by 5 " << std::endl;
}


//*** eine leere Klammer bedeutet, dass die Funktion keine Argumente benötigt
//Gibt - solange im Wertebereich - die näxhste Fibonaccizahl aus
unsigned int get_next_Fibonacci(){
  if(fibonacci1 < (UINT_MAX/2)){
    unsigned int temp = fibonacci2;
    fibonacci2 += fibonacci1;
    fibonacci1 = temp;
    
    return fibonacci1;
  } else {
    std::cout << "Entered count is too big" << std::endl;
    return 0;
  }
}

int main(){
  std::cout << "Enter number of Fibnacci numbers to be calculated: ";
  unsigned int input;
  std::cin >> input;
  
  for(unsigned int i = 1; i <= input; i++){
    //Nächste Fibonaccizahl wird berechnet und ausgegeben. 
    //%%% Aufruf der Funktion get_next_Fibonacci()
    unsigned int next = get_next_Fibonacci();
    //%%% Aufruf der Funktion print_number_with_info(i, next) mit i als Position der Fibonaccizahl und next als number.
    print_number_with_info(i, next);
  }

  return 0;
}
