#include <iostream>

struct Node {
  std::string data; // unsere Data sind vom Typ String
  Node* below = nullptr; // Pointer auf Node unter diesem Node
};

class Stack{
  Node* top_node = nullptr; // Brauchen nur den obersten Node zu speichern.
  
  public:
  //Füge Element zu Stack hinzu
  void push(std::string input);
  //Lösche Element aus dem Stack und gib dessen Wert zurück.
  std::string pop();
};
