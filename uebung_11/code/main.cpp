#include "stack.h"

int main(){
  Stack stack;
  
  do{
    char in;
    std::cin >> in;
    switch (in) {
      case 'q': {
        std::cout << "Stopped" << std::endl;
        return 0;
      }
      case '+': {
        std::cout << "enter new data to be added to stack" << std::endl;
        std::string str;
        std::cin >> str;
        stack.push(str);
        std::cout << "added" << std::endl;
        break;
      }
      case '-': {
        std::cout << stack.pop() << std::endl;
        break;
      }
    }
  }while(1);
}
